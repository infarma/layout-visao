package arquivoDePedido

import (
	"strconv"
	"strings"
)

type DadosDoPedidoRegistro4 struct {
	TipoRegistro         int64  `json:"TipoRegistro"`
	NumeroPeriodoCliente string `json:"NumeroPeriodoCliente"`
	SemUtilizacao        string  `json:"SemUtilizacao"`
}

func GetDadosDoPedidoRegistro4(runes []rune) DadosDoPedidoRegistro4 {
	dadosDoPedidoRegistro4 := DadosDoPedidoRegistro4{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	dadosDoPedidoRegistro4.TipoRegistro = tipoRegistro

	dadosDoPedidoRegistro4.NumeroPeriodoCliente = strings.TrimSpace(string(runes[1:16]))

	dadosDoPedidoRegistro4.SemUtilizacao = strings.TrimSpace(string(runes[16:31]))

	return dadosDoPedidoRegistro4
}
