package arquivoDePedido

import (
	"strconv"
	"strings"
)

type ItensDoPedido struct {
	TipoRegistro         int64   `json:"TipoRegistro"`
	CodigoEANProduto     int64   `json:"CodigoEANProduto"`
	Quantidade           int64   `json:"Quantidade"`
	TipoOcorrencia       int64   `json:"TipoOcorrencia"`
	CodigoProdutoCliente string  `json:"CodigoProdutoCliente"`
	DescontoItem         float64 `json:"DescontoItem"`
}

func GetItensDoPedido(runes []rune) ItensDoPedido {
	itensDoPedido := ItensDoPedido{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	itensDoPedido.TipoRegistro = tipoRegistro

	codigoEANProduto, _ := strconv.ParseInt(string(runes[1:14]), 10, 64)
	itensDoPedido.CodigoEANProduto = codigoEANProduto

	quantidade, _ := strconv.ParseInt(string(runes[14:18]), 10, 64)
	itensDoPedido.Quantidade = quantidade

	tipoOcorrencia, _ := strconv.ParseInt(string(runes[18:20]), 10, 64)
	itensDoPedido.TipoOcorrencia = tipoOcorrencia

	itensDoPedido.CodigoProdutoCliente = strings.TrimSpace(string(runes[20:27]))

	descontoItem, _ := strconv.ParseFloat(string(runes[27:29])+"."+string(runes[29:31]), 64)
	itensDoPedido.DescontoItem = descontoItem

	return itensDoPedido
}
