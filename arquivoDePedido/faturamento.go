package arquivoDePedido

import (
	"strconv"
	"strings"
)

type Faturamento struct {
	TipoRegistro                 int64  `json:"TipoRegistro"`
	TipoPagamento                int64  `json:"TipoPagamento"`
	CodigoPrazoDeterminado       string `json:"CodigoPrazoDeterminado"`
	NumeroDeDiasPrazoDeterminado int64  `json:"NumeroDeDiasPrazoDeterminado"`
	NumeroDoPedidoPrincipal      int64  `json:"NumeroDoPedidoPrincipal"`
	SemUtilizacao                int64  `json:"SemUtilizacao"`
}


func GetFaturamento(runes []rune) Faturamento {
	faturamento := Faturamento{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	faturamento.TipoRegistro = tipoRegistro

	tipoPagamento, _ := strconv.ParseInt(string(runes[1:2]), 10, 64)
	faturamento.TipoPagamento = tipoPagamento

	faturamento.CodigoPrazoDeterminado = strings.TrimSpace(string(runes[2:6]))

	numeroDeDiasPrazoDeterminado, _ := strconv.ParseInt(string(runes[6:9]), 10, 64)
	faturamento.NumeroDeDiasPrazoDeterminado = numeroDeDiasPrazoDeterminado

	numeroDoPedidoPrincipal, _ := strconv.ParseInt(string(runes[9:16]), 10, 64)
	faturamento.NumeroDoPedidoPrincipal = numeroDoPedidoPrincipal

	semUtilizacao, _ := strconv.ParseInt(string(runes[16:31]), 10, 64)
	faturamento.SemUtilizacao = semUtilizacao

	return faturamento
}
