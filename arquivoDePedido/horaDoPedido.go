package arquivoDePedido

import (
	"strconv"
)

type HoraDoPedido struct {
	TipoRegistro  int64 `json:"TipoRegistro"`
	Hora          int64 `json:"Hora"`
	SemUtilizacao int64 `json:"SemUtilizacao"`
}

func GetHoraDoPedido(runes []rune) HoraDoPedido {
	horaDoPedido := HoraDoPedido{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	horaDoPedido.TipoRegistro = tipoRegistro

	hora, _ := strconv.ParseInt(string(runes[1:7]), 10, 64)
	horaDoPedido.Hora = hora

	semUtilizacao, _ := strconv.ParseInt(string(runes[7:31]), 10, 64)
	horaDoPedido.SemUtilizacao = semUtilizacao

	return horaDoPedido
}
