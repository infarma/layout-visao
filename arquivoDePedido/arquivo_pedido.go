package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDeEnvio struct {
	Cabecalho              Cabecalho              `json:"Cabecalho"`
	DadosDoPedido          DadosDoPedido          `json:"DadosDoPedido"`
	Faturamento            Faturamento            `json:"Faturamento"`
	DadosDoPedidoRegistro4 DadosDoPedidoRegistro4 `json:"DadosDoPedidoRegistro4"`
	DataDoPedido           DataDoPedido           `json:"DataDoPedido"`
	HoraDoPedido           HoraDoPedido           `json:"HoraDoPedido"`
	ItensDoPedido          []ItensDoPedido        `json:"ItensDoPedido"`
	Rodape                 Rodape                 `json:"Rodape"`
}

func ConvertToStruct(fileHandle *os.File) ArquivoDeEnvio {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDeEnvio{}
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		if identificador == "01" {
			arquivo.Cabecalho = GetCabecalho(runes)
		} else if identificador == "02" {
			arquivo.DadosDoPedido = GetDadosDoPedido(runes)
		} else if identificador == "03" {
			arquivo.Faturamento = GetFaturamento(runes)
		} else if identificador == "04" {
			arquivo.DadosDoPedidoRegistro4 = GetDadosDoPedidoRegistro4(runes)
		} else if identificador == "05" {
			arquivo.DataDoPedido = GetDataDoPedido(runes)
		} else if identificador == "06" {
			arquivo.HoraDoPedido = GetHoraDoPedido(runes)
		} else if identificador == "07" {
			arquivo.ItensDoPedido = append(arquivo.ItensDoPedido, GetItensDoPedido(runes))
		} else if identificador == "08" {
			arquivo.Rodape = GetRodape(runes)
		}
	}
	return arquivo
}