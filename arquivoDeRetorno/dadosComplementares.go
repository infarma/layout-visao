package arquivoDeRetorno

import (
	"strconv"
	"strings"
)

type DadosComplementares struct {
	TipoRegistro  int64  `json:"TipoRegistro"`
	Data          int64  `json:"Data"`
	Hora          int64  `json:"Hora"`
	SemUtilizacao string `json:"SemUtilizacao"`
}

func GetDadosComplementares(runes []rune) DadosComplementares {
	dadosComplementares := DadosComplementares{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	dadosComplementares.TipoRegistro = tipoRegistro

	data, _ := strconv.ParseInt(string(runes[1:9]), 10, 64)
	dadosComplementares.Data = data

	hora, _ := strconv.ParseInt(string(runes[9:15]), 10, 64)
	dadosComplementares.Hora = hora

	dadosComplementares.SemUtilizacao = strings.TrimSpace(string(runes[23:25]))

	return dadosComplementares
}
