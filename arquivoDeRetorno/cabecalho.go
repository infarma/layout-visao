package arquivoDeRetorno

import (
	"strconv"
	"strings"
)

type Cabecalho struct {
	TipoRegistro     int64 `json:"TipoRegistro"`
	CnpjCliente      string `json:"CnpjCliente"`
	CodigoProjeto    string `json:"CodigoProjeto"`
	PedidoPharmaLink string `json:"PedidoPharmaLink"`
	SemUtilizacao     string `json:"SemUtilizacao"`
}

func GetCabecalho(runes []rune) Cabecalho {
	cabecalho := Cabecalho{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	cabecalho.TipoRegistro = tipoRegistro

	cabecalho.CnpjCliente = strings.TrimSpace(string(runes[1:15]))

	cabecalho.CodigoProjeto = strings.TrimSpace(string(runes[15:16]))

	cabecalho.PedidoPharmaLink = strings.TrimSpace(string(runes[16:23]))

	cabecalho.SemUtilizacao = strings.TrimSpace(string(runes[23:25]))

	return cabecalho
}
