package layout_visao

import (
	"bitbucket.org/infarma/layout-visao/arquivoDePedido"
	"os"
)

func GetArquivoDeEnvio(fileHandle *os.File) arquivoDePedido.ArquivoDeEnvio {
	return arquivoDePedido.ConvertToStruct(fileHandle)
}
