package arquivoDeNotaFiscal

import (
	"strconv"
	"strings"
)

type TotaisDaNotaFiscal struct {
	TipoRegistro       int64   `json:"TipoRegistro"`
	ValorFrete         float64 `json:"ValorFrete"`
	ValorSeguro        float64 `json:"ValorSeguro"`
	OutrasDespesas     float64 `json:"OutrasDespesas"`
	ValorTotalProdutos float64 `json:"ValorTotalProdutos"`
	ValorTotalNF       float64 `json:"ValorTotalNF"`
	ValorIPI           float64 `json:"ValorIPI"`
	Livre              string  `json:"Livre"`
}

func GetTotaisDaNotaFiscal(runes []rune) TotaisDaNotaFiscal {
	totaisDaNotaFiscal := TotaisDaNotaFiscal{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	totaisDaNotaFiscal.TipoRegistro = tipoRegistro

	valorFrete, _ := strconv.ParseFloat(string(runes[1:7])+"."+string(runes[7:9]), 64)
	totaisDaNotaFiscal.ValorFrete = valorFrete

	valorSeguro, _ := strconv.ParseFloat(string(runes[9:15])+"."+string(runes[15:17]), 64)
	totaisDaNotaFiscal.ValorSeguro = valorSeguro

	outrasDespesas, _ := strconv.ParseFloat(string(runes[17:23])+"."+string(runes[23:25]), 64)
	totaisDaNotaFiscal.OutrasDespesas = outrasDespesas

	valorTotalProdutos, _ := strconv.ParseFloat(string(runes[25:31])+"."+string(runes[31:33]), 64)
	totaisDaNotaFiscal.ValorTotalProdutos = valorTotalProdutos

	valorTotalNF, _ := strconv.ParseFloat(string(runes[33:39])+"."+string(runes[39:41]), 64)
	totaisDaNotaFiscal.ValorTotalNF = valorTotalNF

	valorIPI, _ := strconv.ParseFloat(string(runes[41:47])+"."+string(runes[47:49]), 64)
	totaisDaNotaFiscal.ValorIPI = valorIPI

	totaisDaNotaFiscal.Livre = strings.TrimSpace(string(runes[49:80]))

	return totaisDaNotaFiscal
}
