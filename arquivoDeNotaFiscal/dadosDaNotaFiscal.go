package arquivoDeNotaFiscal

import (
	"strconv"
	"strings"
)

type DadosDaNotaFiscal struct {
	TipoRegistro          int64  `json:"TipoRegistro"`
	DataSaidaMercadoria   int64  `json:"DataSaidaMercadoria"`
	HoraSaidaMercadoria   int64  `json:"HoraSaidaMercadoria"`
	DataEmissaoNotaFiscal int64  `json:"DataEmissaoNotaFiscal"`
	CnpjLoja              string `json:"CnpjLoja"`
	NumeroNotaFiscal      int64  `json:"NumeroNotaFiscal"`
	SerieDocumento        string `json:"SerieDocumento"`
	VencimentoNotaFiscal  int64  `json:"VencimentoNotaFiscal"`
	Livre                 string `json:"Livre"`
}

func GetDadosDaNotaFiscal(runes []rune) DadosDaNotaFiscal {
	dadosDaNotaFiscal := DadosDaNotaFiscal{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	dadosDaNotaFiscal.TipoRegistro = tipoRegistro

	dataSaidaMercadoria, _ := strconv.ParseInt(string(runes[1:9]), 10, 64)
	dadosDaNotaFiscal.DataSaidaMercadoria = dataSaidaMercadoria

	horaSaidaMercadoria, _ := strconv.ParseInt(string(runes[9:15]), 10, 64)
	dadosDaNotaFiscal.HoraSaidaMercadoria = horaSaidaMercadoria

	dataEmissaoNotaFiscal, _ := strconv.ParseInt(string(runes[15:29]), 10, 64)
	dadosDaNotaFiscal.DataEmissaoNotaFiscal = dataEmissaoNotaFiscal

	dadosDaNotaFiscal.CnpjLoja = strings.TrimSpace(string(runes[29:30]))

	numeroNotaFiscal, _ := strconv.ParseInt(string(runes[30:37]), 10, 64)
	dadosDaNotaFiscal.NumeroNotaFiscal = numeroNotaFiscal

	dadosDaNotaFiscal.SerieDocumento = strings.TrimSpace(string(runes[37:52]))

	vencimentoNotaFiscal, _ := strconv.ParseInt(string(runes[30:37]), 10, 64)
	dadosDaNotaFiscal.VencimentoNotaFiscal = vencimentoNotaFiscal

	dadosDaNotaFiscal.Livre = strings.TrimSpace(string(runes[52:80]))

	return dadosDaNotaFiscal
}
