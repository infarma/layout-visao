package arquivoDeNotaFiscal

import (
	"strconv"
	"strings"
)

type FimDeArquivo struct {
	TipoRegistro          int64  `json:"TipoRegistro"`
	NumeroItensNotaFiscal int64  `json:"NumeroItensNotaFiscal"`
	Livre                 string `json:"Livre"`
}

func GetFimDeArquivo(runes []rune) FimDeArquivo {
	fimDeArquivo := FimDeArquivo{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	fimDeArquivo.TipoRegistro = tipoRegistro

	numeroItensNotaFiscal, _ := strconv.ParseInt(string(runes[1:5]), 10, 64)
	fimDeArquivo.NumeroItensNotaFiscal = numeroItensNotaFiscal

	fimDeArquivo.Livre = strings.TrimSpace(string(runes[75:80]))

	return fimDeArquivo
}
